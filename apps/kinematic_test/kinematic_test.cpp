#include "kinematic_model.h"
#include <eigen3/Eigen/src/Core/Matrix.h>
#include <iostream>

int main(){
  double L1=0.2, L2=0.4;
  Eigen::Vector2d q(M_PI/3.0,  M_PI_4),X;
  Eigen::Matrix2d J;
  RobotModel Rob(L1,L2);
  Rob.FwdKin(X,J,q);
  // Compute the forward kinematics and jacobian matrix for 
  //      q =  M_PI/3.0,  M_PI_4
  // For a small variation of q, compute the variation on X and check dx = J . dq  

}
