#include "trajectory_generation.h"
#include "iostream"
#include <eigen3/Eigen/src/Core/Matrix.h>

Polynomial::Polynomial(){};

Polynomial::Polynomial(const double &piIn, const double &pfIn, const double & DtIn){
  //TODO initialize the object polynomial coefficients
  a[0]=piIn;
  a[1]=0;
  a[2]=0;
  a[3]=(10/pow(DtIn,3))*(pfIn-piIn);
  a[4]=(-15/pow(DtIn,4))*(pfIn-piIn);
  a[5]=(6/pow(DtIn,5))*(pfIn-piIn);
  pi=piIn;
  pf=pfIn;
  Dt=DtIn;
};

void          Polynomial::update(const double &piIn, const double &pfIn, const double & DtIn){
  //TODO update polynomial coefficients
  a[0]=piIn;
  a[1]=0;
  a[2]=0;
  a[3]=(10/pow(DtIn,3))*(pfIn-piIn);
  a[4]=(-15/pow(DtIn,4))*(pfIn-piIn);
  a[5]=(6/pow(DtIn,5))*(pfIn-piIn);
  pi=piIn;
  pf=pfIn;
  Dt=DtIn;
};

const double  Polynomial::p     (const double &t){
  //TODO compute position
  double position;
  position=a[0]+a[1]*t+a[2]*pow(t,2)+a[3]*pow(t,3)+a[4]*pow(t,4)+a[5]*pow(t,5);
  return position;
};

const double  Polynomial::dp    (const double &t){
  //TODO compute velocity
  double velocity;
  velocity=a[1]+2*a[2]*t+3*a[3]*pow(t,2)+4*a[4]*pow(t,3)+5*a[5]*pow(t,4);
  // std::cout<<"la vitesse est :"<<velocity<<std::endl;
  return velocity;
};

Point2Point::Point2Point(const Eigen::Vector2d & xi, const Eigen::Vector2d & xf, const double & DtIn)
{
  polx.update(xi(0),xf(0),DtIn);
  poly.update(xi(1),xf(1),DtIn);
}

  //TODO initialize object and polynomials

Eigen::Vector2d Point2Point::X(const double & time){
  //TODO compute cartesian position
Eigen::Vector2d point;
point(0)=polx.p(time);
point(1)=poly.p(time);
  return point;
}

Eigen::Vector2d Point2Point::dX(const double & time){
  //TODO compute cartesian velocity
  Eigen::Vector2d dpoint;
  dpoint(0)=polx.dp(time);
  dpoint(1)=poly.dp(time);
  return dpoint;
}